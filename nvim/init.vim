call plug#begin('~/.local/share/nvim/plugged')

Plug 'https://github.com/pangloss/vim-javascript'
Plug 'mxw/vim-jsx'

Plug 'itchyny/lightline.vim'

Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'w0rp/ale'

Plug 'alvan/vim-closetag'
Plug 'jiangmiao/auto-pairs'

Plug 'connorholyday/vim-snazzy'

call plug#end()

set t_Co=256
let g:SnazzyTransparent = 1
colorscheme snazzy

let g:deoplete#enable_at_startup = 1

set number
set tabstop=2 shiftwidth=2 expandtab
set noswapfile

set shell=/usr/local/bin/zsh

let g:closetag_filenames='*.xml,*.html,*.xhtml,*.jsx'
let g:closetag_xhtml_filenames='*.xhtml,*.jsx'

nmap <C-P> :FZF<CR>
nnoremap <silent> <C-H> :tabprevious<CR>
nnoremap <silent> <C-L> :tabnext<CR>
